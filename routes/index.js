var express = require('express');
var fs = require('fs');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile('lab6.html',{root:'public'});
  //res.render('index', { title: 'Express' });
});

router.get('/getcities', function(req,res) {
  var myRe = new RegExp("^" + req.query.q);
  var rval = [];
  fs.readFile(__dirname + '/cities.dat.txt',function(err,data) {
    if(err) throw err;
    var cities = data.toString().split("\n");
    for(var i = 0; i < cities.length; i++) {
      var result = cities[i].search(myRe);
      if(result == 0 )rval.push({city:cities[i]});
    }
    res.status(200).json(rval);
  })
});

module.exports = router;
